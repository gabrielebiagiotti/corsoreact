import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

//mount point dell'applicazione react, che monta il primo componente parametro nel contenitore che è il secondo parametro
ReactDOM.render(<App /> , document.getElementById('root'));
registerServiceWorker();