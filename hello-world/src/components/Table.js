import React, { Component } from 'react';
import './Table.css';

// https://reactjs.org/docs/react-component.html
class Table extends Component{
   
    render(){
        let secondoHeader = "Secondo header";
        return(
            <table>
                <thead>
                    <tr colSpan="2" className="tableHeader"><th>{this.props.titolo}</th></tr>
                    <tr colSpan="2" className="tableHeader"><th>{secondoHeader}</th></tr>
                </thead>
                <thead>
                    <tr>
                    <th>
                        Colonna 1
                    </th>
                    <th>
                        Colonna 2
                    </th>
                    </tr>
                </thead>
               <tbody>
               <tr>
                <td>Valore colonna 1</td>
                <td>Valore colonna 2</td>
                </tr>
               </tbody>
                
            </table>
        );
    }
}

export default Table