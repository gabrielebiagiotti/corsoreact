import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Table from './components/Table'

class App extends Component {
  render() {
    return (
      //usare className invece di class come in HTML (perchè class è una keyword)
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <Table titolo="Titolo della table"/>
      </div>
      
    );
  }
}

export default App;
